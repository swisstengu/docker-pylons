FROM fedora:28
MAINTAINER swisstengu <tengu@tengu.ch>

RUN dnf update -y
RUN dnf install -y python-pylons \
    python2-mysql \
    python2-flup

RUN dnf clean all

COPY entrypoint /entrypoint
RUN chmod +x /entrypoint

RUN useradd -d /webapp --uid 1024 pylons
USER pylons

CMD ["/entrypoint"]
